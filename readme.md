Rezervační systém služebních vozidel
====================================

Úkolem je implementovat formulář pro rezervaci služebních vozidel a stránku se seznamem aktuálních rezervací.

Kostra projektu obsahuje prázdý ReservationsPresenter, který bude stránky pro výpis rezervací
a práci s nimi obsluhovat. Přihlašování uživatelů není potřeba řešit,
pro jednoduchost je vždy přihlášen první uživatel v db (metoda fakeLogin v BasePresenteru).
Pro uživatele a vozidla jsou vytvořeny migrace a seedry pomocí Phinx (http://docs.phinx.org/en/latest/)
a entity v Doctrine ORM (User a Car). Implementace datové vrstvy rezervací je na vás. Použití Doctrine ORM
není povinné, můžete případně nahradit jiným řešením.

Layout je připraven v Bootstrap 4 (http://getbootstrap.com/). Samotný layout není při implementaci
nejdůležitější, vystačíte si se základními prvky Bootstrapu.

Součástí projektu je git repozitář. V rámci implementace ho prosím používejte a commitujte své změny.


Instalace
---------
Před začátkem implementace je potřeba inicializovat sqlite databázi a naplnit ji základními daty.
Systém ji vygeneruje po spuštění migrací:

	composer install
	./vendor/bin/phinx migrate
	./vendor/bin/phinx seed:run -s UsersSeeder -s CarsSeeder

Po dokončení migrace a seedování by již mělo jít zobrazit stránku rezervací. Ta je prázdná,
vypisuje pouze aktuálně přihlášeného uživatele. V databázi budou vytvořeni testovací uživatelé
a vozidla.


Výpis rezervací
---------------
Vytvořte stránkovaný seznam rezervací všech uživatelů, seřazený podle data začátku rezervace vzestupně.
Zobrazeny budou pouze aktuální nebo budoucí rezervace. Vlastní rezervace může uživatel editovat nebo mazat.


Formulář
---------
Uživatel vybírá vozidlo, začátek a konec rezervace (datum a čas). Samozřejmě není možné rezervovat jedno 
vozidlo na stejný časový úsek více uživateli. 
