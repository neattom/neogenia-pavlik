<?php

use Phinx\Migration\AbstractMigration;

class CreateCarsTable extends AbstractMigration
{
    public function change()
    {
		$this->table('car')
			->addColumn('name', 'string')
			->addColumn('license_number', 'string')
			->addColumn('created_at', 'datetime')
			->addColumn('updated_at', 'datetime')
			->create();
    }
}
