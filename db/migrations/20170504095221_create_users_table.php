<?php

use Phinx\Migration\AbstractMigration;

class CreateUsersTable extends AbstractMigration
{
    public function change()
    {
		$this->table('user')
			->addColumn('name', 'string')
			->addColumn('surname', 'string')
			->addColumn('email', 'string')
			->addColumn('created_at', 'datetime')
			->addColumn('updated_at', 'datetime')
			->create();
    }
}
