<?php

use Phinx\Migration\AbstractMigration;

class CreateReservationTable extends AbstractMigration
{
    public function change()
    {
        $this->table('reservation')
            ->addColumn('car_id', 'integer')
            ->addColumn('reservation_from', 'datetime')
            ->addColumn('reservation_to', 'datetime')
            ->addColumn('created_at', 'datetime')
            ->addColumn('created_by', 'integer')
            ->addColumn('updated_at', 'datetime')
            ->create();
    }
}
