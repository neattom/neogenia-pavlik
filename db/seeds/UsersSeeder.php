<?php

use Phinx\Seed\AbstractSeed;

class UsersSeeder extends AbstractSeed
{
	public function run()
	{
		$data = [
			[
				'id' => '1',
				'name' => 'Karel',
				'surname' => 'Novák',
				'email' => 'karel.novak@neogenia.cz',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
			],
			[
				'id' => '2',
				'name' => 'Pavel',
				'surname' => 'Novotný',
				'email' => 'pavel.novotny@neogenia.cz',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
			],
		];

		$this->table('user')
			->insert($data)
			->save();
	}
}
