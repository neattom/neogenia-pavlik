<?php

use Phinx\Seed\AbstractSeed;

class CarsSeeder extends AbstractSeed
{
    public function run()
    {
		$data = [
			[
				'name' => 'Škoda Octavia',
				'license_number' => '1A1 1234',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Škoda Superb',
				'license_number' => '1A1 5678',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Ford Focus',
				'license_number' => '2A2 1234',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Porsche 911',
				'license_number' => '2A2 5678',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
			],
		];

		$this->table('car')
			->insert($data)
			->save();
    }
}
