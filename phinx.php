<?php
$container = require __DIR__ . '/app/bootstrap.php';

$parameters = $container->getParameters();

return [
	'paths' => [
		'migrations' => 'db/migrations',
		'seeds' => 'db/seeds'
	],
	'environments' => [
		'default_migration_table' => 'migrations',
		'default_database' => 'sqlite',
		'sqlite' => [
			'adapter' => 'sqlite',
			'name' => $parameters['dbname'],
			'charset' => 'utf8',
			'collation' => 'utf8_unicode_ci',
		],
	],
];
