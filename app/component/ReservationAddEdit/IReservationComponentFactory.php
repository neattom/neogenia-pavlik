<?php

namespace App\Component\ReservationAddEdit;

use App\Model\Entity\Reservation;

/**
 * Interface IReservationComponentFactory
 * @package App\Component\Reservation
 */
interface IReservationComponentFactory
{
    /**
     * @param Reservation $reservation
     *
     * @return ReservationComponent
     */
    public function create(Reservation $reservation);
}
