<?php

namespace App\Component\ReservationAddEdit;

use App\Model\Entity\Car;
use App\Model\Entity\Reservation;
use App\Model\Factory\IReservationFactory;
use App\Model\Repository\CarRepository;
use App\Model\Repository\ReservationRepository;
use DateTime;
use Exception;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

/**
 * Class ReservationComponent
 * @package App\Component\Reservation
 * @method onError($message)
 * @method onSuccess(Reservation $reservation)
 */
class ReservationComponent extends Control
{
    /**
     * @var CarRepository
     */
    private $carRepository;

    /**
     * @var Reservation
     */
    private $reservation;

    /**
     * @var IReservationFactory
     */
    private $reservationFactory;

    /**
     * @var ReservationRepository
     */
    private $reservationRepository;

    /**
     * @var callable
     */
    public $onError = [];

    /**
     * @var callable
     */
    public $onSuccess = [];

    /**
     * ReservationComponent constructor.
     *
     * @param Reservation           $reservation
     * @param CarRepository         $carRepository
     * @param IReservationFactory   $reservationFactory
     * @param ReservationRepository $reservationRepository
     */
    public function __construct(
        Reservation $reservation,
        CarRepository $carRepository,
        IReservationFactory $reservationFactory,
        ReservationRepository $reservationRepository
    ) {
        $this->carRepository = $carRepository;
        $this->reservation = $reservation;
        $this->reservationFactory = $reservationFactory;
        $this->reservationRepository = $reservationRepository;
    }

    /**
     * @return array
     */
    private function getCarList()
    {
        $carList = [];

        foreach ($this->carRepository->findAll() as $car) {
            $carList[$car->getId()] = $car->getName() . ' (' . $car->getLicenseNumber() . ')';
        }

        return $carList;
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form();

        $form->addText('reservationFrom', 'Od')
            ->setDefaultValue(null !== $this->reservation->getReservationFrom() ?
                $this->reservation->getReservationFrom()->format('d.m.Y H:i') : '')
            ->setRequired('Vyberte datum rezervace od!');

        $form->addText('reservationTo', 'Do')
            ->setDefaultValue(null !== $this->reservation->getReservationTo() ?
                $this->reservation->getReservationTo()->format('d.m.Y H:i') : '')
            ->setRequired('Vyberte datum rezervace do!');

        $form->addSelect('car', 'Vozidlo', [0 => '-- Vyberte vozidlo --'] + $this->getCarList())
            ->setRequired(true)
            ->setDefaultValue(null !== $this->reservation->getCar() ?
                $this->reservation->getCar()->getId() : 0)
            ->addRule(Form::NOT_EQUAL, 'Vyberte nějaké vozidlo!', 0);

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = [$this, 'processForm'];

        return $form;
    }

    /**
     * @param Form $form
     */
    public function processForm(Form $form)
    {
        $values = $form->getValues();

        try {
            if (false === $dateFrom = strtotime($values->reservationFrom)) {
                throw new Exception();
            }

            $reservationFrom = new DateTime(date('Y-m-d H:i:00', $dateFrom));
        } catch (Exception $e) {
            $this->onError('Chybné datum rezervace od!');
            return;
        }

        try {
            if (false === $dateTo = strtotime($values->reservationTo)) {
                throw new Exception();
            }
            $reservationTo = new DateTime(date('Y-m-d H:i:00', $dateTo));
        } catch (Exception $e) {
            $this->onError('Chybné datum rezervace do!');
            return;
        }

        if ($reservationTo->getTimestamp() <= $reservationFrom->getTimestamp()) {
            $this->onError('Chybné datum rezervace od-do!');
            return;
        }

        /** @var Car|null $car */
        if (null === $car = $this->carRepository->find($values->car)) {
            $this->onError('Dané vozidlo neexistuje!');
            return;
        }

        $this->reservation->setCar($car);
        $this->reservation->setReservationFrom($reservationFrom);
        $this->reservation->setReservationTo($reservationTo);

        if (false === $this->reservationRepository->validateReservation($this->reservation)) {
            $this->onError('V daném termínu nelze vozidlo rezervovat, již je rezervované někým jiným!');
            return;
        }

        $this->onSuccess($this->reservation);
    }

    public function render()
    {
        $this->template->setFile(__DIR__ . '/default.latte');
        $this->template->render();
    }
}
