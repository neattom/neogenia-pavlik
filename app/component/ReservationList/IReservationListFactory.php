<?php

namespace App\Component\ReservationList;

/**
 * Interface IReservationListFactory
 * @package App\Component\ReservationList
 */
interface IReservationListFactory
{
    /**
     * @param int $page
     * @param int $user
     *
     * @return ReservationList
     */
    public function create($page, $user);
}
