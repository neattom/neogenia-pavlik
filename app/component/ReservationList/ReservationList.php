<?php

namespace App\Component\ReservationList;

use App\Model\Repository\ReservationRepository;
use Nette\Application\UI\Control;

/**
 * Class ReservationList
 * @package App\Component\ReservationList
 */
class ReservationList extends Control
{
    /**
     * @var int
     */
    private $page;

    /**
     * @var ReservationRepository
     */
    private $reservationRepository;

    /**
     * @var int
     */
    private $user;

    /**
     * ReservationList constructor.
     *
     * @param int                   $page
     * @param int                   $user
     * @param ReservationRepository $reservationRepository
     */
    public function __construct(
        $page,
        $user,
        ReservationRepository $reservationRepository
    ) {
        $this->page = $page;
        $this->reservationRepository = $reservationRepository;
        $this->user = $user;
    }

    public function render()
    {
        $reservationList = $this->reservationRepository->findAllActive($this->page);
        $totalCount = count($reservationList);

        $this->template->count = count($reservationList->getIterator()->getArrayCopy());
        $this->template->firstPage = 1 === $this->page;
        $this->template->lastPage = ($this->page * ReservationRepository::LIMIT) >= $totalCount;
        $this->template->limit = ReservationRepository::LIMIT;
        $this->template->page = $this->page;
        $this->template->reservationList = $reservationList;
        $this->template->totalCount = $totalCount;
        $this->template->user = $this->user;
        $this->template->setFile(__DIR__ . '/default.latte');
        $this->template->render();
    }
}
