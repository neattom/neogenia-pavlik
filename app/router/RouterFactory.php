<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
    use Nette\StaticClass;

    /**
     * @return Nette\Application\IRouter
     */
    public static function createRouter()
    {
        $router = new RouteList;
        $router[] = new Route('rezervace/nova', 'ReservationAdd:default');
        $router[] = new Route('rezervace/editace/<id>', 'ReservationEdit:default');
        $router[] = new Route('rezervace[/<page>]', 'Reservations:default');
        $router[] = new Route('<presenter>[/<action>][/<id>]', 'Reservations:default');
        return $router;
    }
}
