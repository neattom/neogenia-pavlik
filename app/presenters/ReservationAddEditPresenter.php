<?php

namespace App\Presenters;

use App\Component\ReservationAddEdit\IReservationComponentFactory;
use App\Component\ReservationAddEdit\ReservationComponent;
use App\Model\Entity\Reservation;
use App\Model\Entity\User;
use App\Model\Factory\IReservationFactory;
use App\Model\Repository\CarRepository;
use App\Model\Repository\ReservationRepository;
use App\Model\Repository\UserRepository;
use Exception;
use Kdyby\Doctrine\EntityManager;

/**
 * Class ReservationAddEditPresenter
 * @package App\Presenters
 */
abstract class ReservationAddEditPresenter extends BasePresenter
{
    /**
     * @var CarRepository
     */
    protected $carRepository;

    /**
     * @var null|Reservation
     */
    protected $reservation;

    /**
     * @var IReservationComponentFactory
     */
    protected $reservationComponentFactory;

    /**
     * @var IReservationFactory
     */
    protected $reservationFactory;

    /**
     * @var ReservationRepository
     */
    protected $reservationRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * ReservationAddEditPresenter constructor.
     *
     * @param CarRepository                $carRepository
     * @param EntityManager                $em
     * @param IReservationComponentFactory $reservationComponentFactory
     * @param IReservationFactory          $reservationFactory
     * @param ReservationRepository        $reservationRepository
     * @param UserRepository               $userRepository
     */
    public function __construct(
        CarRepository $carRepository,
        EntityManager $em,
        IReservationComponentFactory $reservationComponentFactory,
        IReservationFactory $reservationFactory,
        ReservationRepository $reservationRepository,
        UserRepository $userRepository
    ) {
        parent::__construct();
        $this->carRepository = $carRepository;
        $this->em = $em;
        $this->reservationComponentFactory = $reservationComponentFactory;
        $this->reservationFactory = $reservationFactory;
        $this->reservationRepository = $reservationRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @return ReservationComponent
     */
    public function createComponentReservationForm()
    {
        /** @var User|null $user */
        $user = $this->userRepository->find($this->user->getId());

        /** @var Reservation $reservation */
        $reservation = isset($this->reservation) ? $this->reservation :
            $this->reservationFactory->create($user);

        $form = $this->reservationComponentFactory->create($reservation);

        $form->onError[] = function ($message) {
            $this->flashMessage($message, 'danger');
        };

        $form->onSuccess[] = function (Reservation $reservation) {
            try {
                $this->em->persist($reservation);
                $this->em->flush();
                $this->flashMessage('Rezervace byla úspěšně uložena.', 'success');
            } catch (Exception $e) {
                $this->flashMessage('Rezervaci se nepodařilo uložit. (' . $e->getMessage() . ')', 'danger');
            }
            $this->redirect('Reservations:');
        };

        return $form;
    }
}
