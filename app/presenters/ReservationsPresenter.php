<?php

namespace App\Presenters;

use App\Component\ReservationList\IReservationListFactory;
use App\Component\ReservationList\ReservationList;
use App\Model\Repository\ReservationRepository;
use App\Model\Repository\UserRepository;

/**
 * Class ReservationsPresenter
 * @package App\Presenters
 */
class ReservationsPresenter extends BasePresenter
{
    /**
     * @var int
     */
    private $page = 1;

    /**
     * @var IReservationListFactory
     */
    private $reservationListFactory;

    /**
     * @var ReservationRepository
     */
    private $reservationRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ReservationsPresenter constructor.
     *
     * @param IReservationListFactory $reservationListFactory
     * @param ReservationRepository   $reservationRepository
     * @param UserRepository          $userRepository
     */
    public function __construct(
        IReservationListFactory $reservationListFactory,
        ReservationRepository $reservationRepository,
        UserRepository $userRepository
    ) {
        parent::__construct();
        $this->reservationListFactory = $reservationListFactory;
        $this->reservationRepository = $reservationRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @return ReservationList
     */
    protected function createComponentReservationList()
    {
        return $this->reservationListFactory->create($this->page, $this->user->getId());
    }

    /**
     * @param $page
     */
    public function actionDefault($page)
    {
        $page = intval($page);
        $this->page = 0 >= $page ? 1 : $page;
    }
}
