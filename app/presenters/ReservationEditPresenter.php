<?php

namespace App\Presenters;

use App\Model\Entity\Reservation;
use Nette\Application\BadRequestException;

/**
 * Class ReservationEditPresenter
 * @package App\Presenters
 */
class ReservationEditPresenter extends ReservationAddEditPresenter
{
    /**
     * @param $id
     *
     * @throws BadRequestException
     */
    public function actionDefault($id)
    {
        if (null === $reservation = $this->reservationRepository->find($id)) {
            throw new BadRequestException();
        }

        /** @var Reservation $reservation */
        if ($this->user->getId() !== $reservation->getCreatedBy()->getId()) {
            $this->flashMessage('Nemáte oprávnění pro editaci této rezervace!', 'danger');
        }

        $this->reservation = $reservation;
    }
}
