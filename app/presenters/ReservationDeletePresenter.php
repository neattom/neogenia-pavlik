<?php

namespace App\Presenters;

use Exception;
use Nette\Application\AbortException;

/**
 * Class ReservationDeletePresenter
 * @package App\Presenters
 */
class ReservationDeletePresenter extends ReservationAddEditPresenter
{
    /**
     * @param $id
     *
     * @throws AbortException
     */
    public function actionDefault($id)
    {
        if (null !== $reservation = $this->reservationRepository->find(intval($id))) {
            if ($this->user->getId() !== $reservation->getCreatedBy()->getId()) {
                $this->flashMessage('Nemáte oprávnění pro smazání této rezervace!', 'danger');
            } else {
                try {
                    $this->em->remove($reservation);
                    $this->em->flush();
                    $this->flashMessage('Rezervace byla úspěšně smazána.', 'success');
                } catch (Exception $e) {
                    $this->flashMessage('Rezervace se nepodařilo smazat.', 'danger');
                }
            }
        }

        $this->redirect('Reservations:');
    }
}
