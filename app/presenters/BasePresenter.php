<?php

namespace App\Presenters;

use App\Model\Entity\User;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Presenter;
use Nette\Security\AuthenticationException;
use Nette\Security\Identity;

class BasePresenter extends Presenter
{
    /** @var EntityManager @inject */
    public $em;

    /**
     * @throws AuthenticationException
     */
    private function fakeLogin()
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)
            ->find(1);

        $this->getPresenter()->getUser()->login(
            new Identity($user->getId(), null, [
                'name' => $user->getName(),
                'surname' => $user->getSurname(),
                'email' => $user->getEmail(),
            ])
        );
    }

    protected function startup()
    {
        parent::startup();

        if (!$this->getUser()->isLoggedIn()) {
            $this->fakeLogin();
        }
    }
}
