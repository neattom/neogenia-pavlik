<?php

namespace App\Model\Repository;

use App\Model\Entity\Reservation;
use DateTime;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Kdyby\Doctrine\EntityRepository;

/**
 * Class ReservationRepository
 */
class ReservationRepository extends EntityRepository
{
    const LIMIT = 10;

    /**
     * @param int $page
     *
     * @return Paginator
     */
    public function findAllActive($page = 1)
    {
        return new Paginator(
            $this->_em->createQueryBuilder()
                ->select('reservation', 'car', 'user')
                ->from(Reservation::class, 'reservation')
                ->leftJoin('reservation.car', 'car')
                ->leftJoin('reservation.createdBy', 'user')
                ->andWhere('reservation.reservationTo >= :now')
                ->addOrderBy('reservation.reservationFrom')
                ->setParameter('now', new DateTime())
                ->setFirstResult(($page - 1) * self::LIMIT)
                ->setMaxResults(self::LIMIT)
                ->getQuery()
        );
    }

    /**
     * @param Reservation $reservation
     *
     * @return bool
     */
    public function validateReservation(Reservation $reservation)
    {
        $query = $this->_em->createQueryBuilder()
            ->select('PARTIAL reservation.{id}')
            ->from(Reservation::class, 'reservation')
            ->andWhere('reservation.car = :car')
            ->andWhere(':reservationFrom < reservation.reservationTo AND :reservationTo > reservation.reservationFrom')
            ->setParameter('car', $reservation->getCar())
            ->setParameter('reservationFrom', $reservation->getReservationFrom())
            ->setParameter('reservationTo', $reservation->getReservationTo());

        if (null !== $reservation->getId()) {
            $query->andWhere('reservation.id != :reservationId')
                ->setParameter('reservationId', $reservation->getId());
        }

        $result = $query->getQuery()->getResult();

        return 0 === count($result);
    }
}
