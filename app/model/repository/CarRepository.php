<?php

namespace App\Model\Repository;

use App\Model\Entity\Car;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;

/**
 * Class CarRepository
 */
class CarRepository extends EntityRepository
{
    /**
     * CarRepository constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        parent::__construct($em, $em->getClassMetadata(Car::class));
    }

    /**
     * @return Car[]
     */
    public function findAll()
    {
        return $this->_em->createQueryBuilder()
            ->select('car')
            ->from(Car::class, 'car')
            ->addOrderBy('car.name')
            ->addOrderBy('car.licenseNumber')
            ->getQuery()
            ->getResult();
    }

}
