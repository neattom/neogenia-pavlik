<?php

namespace App\Model\Entity;

use DateTime;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Model\Repository\ReservationRepository")
 * @ORM\Table(name="reservation")
 * @ORM\HasLifecycleCallbacks()
 */
class Reservation
{
    use Identifier;

    /**
     * @var Car|null
     * @ORM\ManyToOne(targetEntity="Car")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $car;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="CASCADE", name="created_by")
     */
    private $createdBy;

    /**
     * @var DateTime|null
     * @ORM\Column(type="datetime")
     */
    private $reservationFrom;

    /**
     * @var DateTime|null
     * @ORM\Column(type="datetime")
     */
    private $reservationTo;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * Reservation constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->createdAt = new DateTime();
        $this->createdBy = $user;
        $this->updatedAt = new DateTime();
    }

    /**
     * @param PreUpdateEventArgs $event
     * @ORM\PreUpdate()
     */
    public function checkUpdates(PreUpdateEventArgs $event)
    {
        if (0 < count($event->getEntityChangeSet())) {
            $this->updatedAt = new DateTime();
        }
    }

    /**
     * @return Car|null
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * @param Car|null $car
     *
     * @return Reservation
     */
    public function setCar($car)
    {
        $this->car = $car;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @return DateTime|null
     */
    public function getReservationFrom()
    {
        return $this->reservationFrom;
    }

    /**
     * @param DateTime|null $reservationFrom
     *
     * @return Reservation
     */
    public function setReservationFrom($reservationFrom)
    {
        $this->reservationFrom = $reservationFrom;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getReservationTo()
    {
        return $this->reservationTo;
    }

    /**
     * @param DateTime|null $reservationTo
     *
     * @return Reservation
     */
    public function setReservationTo($reservationTo)
    {
        $this->reservationTo = $reservationTo;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

}
