<?php

namespace App\Model\Factory;

use App\Model\Entity\Reservation;
use App\Model\Entity\User;

/**
 * Interface IReservationFactory
 * @package App\Model\Factory
 */
interface IReservationFactory
{
    /**
     * @param User $user
     *
     * @return Reservation
     */
    public function create(User $user);
}
